<?php


namespace Lekurde\TwitBundle\Model;


use PHPUnit\Framework\TestCase;

class TermSearchObjectTest extends TestCase
{
    private TermSearchObject $searchObject;

    public function setUp()
    {
        $this->searchObject = new TermSearchObject();
    }

    /**
     * @group search
     * @dataProvider urlProvider
     */
    public function testTermSearch(string $term, string $lang, string $resultUrl)
    {
        $this->searchObject
            ->setTerm($term)
            ->setLang($lang);

        $url = $this->searchObject->buildUrl();

        self::assertEquals($resultUrl, $url);
    }

    /**
     * @return \string[][]
     */
    public function urlProvider()
    {
        return [
          ['test', 'FR', 'https://api.twitter.com/2/tweets/search/recent?query=test%20lang%3Afr%20-is%3Aretweet'],
          ['coucou', 'fr', 'https://api.twitter.com/2/tweets/search/recent?query=coucou%20lang%3Afr%20-is%3Aretweet'],
          ['another test', 'FR', 'https://api.twitter.com/2/tweets/search/recent?query=another%20test%20lang%3Afr%20-is%3Aretweet']
        ];
    }
}