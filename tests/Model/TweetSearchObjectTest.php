<?php


namespace Lekurde\TwitBundle\Model;


use PHPUnit\Framework\TestCase;

class TweetSearchObjectTest extends TestCase
{
    private TweetSearchObject $searchObject;

    public function setUp()
    {
        $this->searchObject = new TweetSearchObject();
    }

    /**
     * @group search
     * @dataProvider urlProvider
     */
    public function testTermSearch(array $ids, string $resultUrl)
    {
        $this->searchObject
            ->setIds($ids);

        $url = $this->searchObject->buildUrl();

        self::assertEquals($resultUrl, $url);
    }

    /**
     * @return \string[][]
     */
    public function urlProvider()
    {
        return [
          [[1], 'https://api.twitter.com/2/tweets?ids=1'],
          [[1,2], 'https://api.twitter.com/2/tweets?ids=1,2']
        ];
    }
}