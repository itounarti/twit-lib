<?php


namespace Lekurde\TwitBundle\Client;


use Lekurde\TwitBundle\Model\TermSearchObject;
use Lekurde\TwitBundle\Model\TweetSearchObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\CurlHttpClient;

class TwitterClientTest extends TestCase
{
    private TwitterClient $client;

    public function setUp()
    {
        if (false === $token = getenv('BEARER_TOKEN')){
            $token = $_SERVER['BEARER_TOKEN'];
        }

        $this->client = new TwitterClient(new CurlHttpClient(), $token);
    }

    /**
     * @group client
     */
    public function testSearchByTerm()
    {
         $search = (new TermSearchObject())
            ->setTerm('zemmour');

        $results = $this->client->search($search);

        self::assertCount(10, $results['data']);

        foreach ($results['data'] as $result) {
            self::assertArrayHasKey('id', $result);
            self::assertArrayHasKey('text', $result);
        }
    }

    /**
     * @group client
     */
    public function testSearchByTweetId()
    {
        $ids = [1261326399320715264,1278347468690915330];

        $search = (new TweetSearchObject())
            ->setIds($ids);

        $results = $this->client->search($search);

        self::assertCount(2, $results['data']);

        foreach ($results['data'] as $i => $result) {
            self::assertEquals($result['id'], $ids[$i]);

            self::assertArrayHasKey('id', $result);
            self::assertArrayHasKey('text', $result);
        }
    }
}