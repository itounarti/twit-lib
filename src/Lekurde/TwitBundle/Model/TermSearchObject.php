<?php


namespace Lekurde\TwitBundle\Model;


class TermSearchObject implements SearchObjectInterface
{
    private const SERVICE = '/tweets/search/recent';
    private string $term;
    private string $lang = SearchObjectInterface::LANG_FR;

    /**
     * @return string
     */
    public function getTerm(): string
    {
        return $this->term;
    }

    /**
     * @param string $term
     * @return TermSearchObject
     */
    public function setTerm(string $term): TermSearchObject
    {
        $this->term = $term;

        return $this;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return TermSearchObject
     */
    public function setLang(string $lang): TermSearchObject
    {
        $this->lang = $lang;

        return $this;
    }

    public function buildUrl(): string
    {
        return self::API_ENDPOINT . self::SERVICE . '?query=' . rawurlencode($this->getTerm().' lang:'.strtolower($this->getLang()).' -is:retweet');
    }
}