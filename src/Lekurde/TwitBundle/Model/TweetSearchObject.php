<?php


namespace Lekurde\TwitBundle\Model;


class TweetSearchObject implements SearchObjectInterface
{
    private const SERVICE = '/tweets';
    private array $ids;

    /**
     * @return array
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @param array $ids
     * @return TweetSearchObject
     */
    public function setIds(array $ids): TweetSearchObject
    {
        $this->ids = $ids;
        return $this;
    }

    public function buildUrl(): string
    {
        return self::API_ENDPOINT . self::SERVICE . '?ids=' . implode(',', $this->getIds());
    }
}