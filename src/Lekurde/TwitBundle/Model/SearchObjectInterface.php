<?php


namespace Lekurde\TwitBundle\Model;


interface SearchObjectInterface
{
    public const API_ENDPOINT = 'https://api.twitter.com/2';

    public const LANG_FR = 'fr';

    public function buildUrl(): string;
}