<?php


namespace Lekurde\TwitBundle\Client;


use Lekurde\TwitBundle\Model\SearchObjectInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class TwitterClient
{
    private HttpClientInterface $client;
    private string $bearerToken;

    public function __construct(HttpClientInterface $client, string $bearerToken)
    {
        $this->client = $client;
        $this->bearerToken = $bearerToken;
    }

    public function search(SearchObjectInterface $searchObject): array
    {
        $url = $searchObject->buildUrl();

        $response = $this->call($url, 'GET');

        return json_decode($response->getContent(), true);
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private final function call(string $url, string $method = 'GET', array $options = []): ResponseInterface
    {
        $options = array_merge($options, ['headers' =>
            [
                'Authorization' => "Bearer " . $this->bearerToken
            ]
        ]);

        $response = $this->client->request(
            $method,
            $url,
            $options
        );

        if (200 !== $response->getStatusCode()) {
            throw new \Exception(sprintf('Something went wrong for call %s "%s"', $method, $url));
        }

        return $response;
    }
}